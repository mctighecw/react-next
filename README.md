# README

A mini project using Next.js (and React) for server side rendering (SSR).

I started it using the Next.js [tutorial](https://nextjs.org/learn/basics/getting-started).

## App Information

App Name: react-next

Created: March 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/react-next)

## Tech Stack

* Next.js
* React
* Less
* Node.js Express server

## Development and Production

1. To set up and run (development)

  Install Node modules and start

  ```
  $ npm install
  $ npm run dev
  ```

2. To analyze the production server & client bundles

  ```
  $ npm run analyze
  ```

3. To build and run (production)

  ```
  $ npm run build
  $ npm run export
  $ npm run start
  ```

Last updated: 2024-08-13
