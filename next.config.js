const withLess = require('@zeit/next-less');
const withBundleAnalyzer = require("@zeit/next-bundle-analyzer");

module.exports = withLess(withBundleAnalyzer({
    // bundle analysis
    analyzeServer: ["server", "both"].includes(process.env.BUNDLE_ANALYZE),
    analyzeBrowser: ["browser", "both"].includes(process.env.BUNDLE_ANALYZE),
    bundleAnalyzerConfig: {
      server: {
        analyzerMode: 'static',
        reportFilename: '../bundles/server.html'
      },
      browser: {
        analyzerMode: 'static',
        reportFilename: '../bundles/client.html'
      }
    },

    // css modules
    cssModules: false,

    // custom webpack config
    webpack: (config, options) => {
      return config;
    },

    // production build
    distDir: 'build',
    env: {
      ENV: 'production',
      KEY: 'secret'
    },
    exportPathMap: () => {
      return {
        '/': { page: '/' },
        '/about': { page: '/about' },
        '/blog': { page: '/blog' },
        '/shows': { page: '/shows' }
      }
    }
  })
);
