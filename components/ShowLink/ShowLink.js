import Link from 'next/link';

const ShowLink = props => {
  <li key={props.id}>
    <Link href={`/show?id=${props.show.id}`} as={`/show/${props.show.id}`}>
      <a>{props.show.name}</a>
    </Link>
  </li>
}

export default ShowLink;
