import Link from 'next/link';
import './styles.less';

const Header = () => (
  <div>
    <Link href="/">
      <a className="link">Home</a>
    </Link>
    <Link href="/about">
      <a className="link">About</a>
    </Link>
    <Link href="/blog">
      <a className="link">Blog</a>
    </Link>
    <Link href="/shows">
      <a className="link">Shows</a>
    </Link>
  </div>
);

export default Header;
