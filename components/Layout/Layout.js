import Header from '../Header/Header';
import './styles.less';

const Layout = props => (
  <div className="layout">
    <Header />
    {props.children}
  </div>
);

export default Layout;
