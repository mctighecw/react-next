import Link from 'next/link';

const PostLink = props => (
  <li key={props.id}>
    <Link href={`/post?title=${props.title}&text=${props.text}`} as={`/post/${props.id}`}>
      <a>{props.title}</a>
    </Link>
  </li>
)

export default PostLink;
