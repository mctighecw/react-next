import React from 'react';
import './styles.less';

const DropDown = ({ value, options, onChangeMethod }) => (
  <div className="dropdown">
    <select value={value} onChange={onChangeMethod}>
      {options.length > 0 &&
        options.map((item, index) =>
          <option value={item.value} key={index}>
            {item.label}
          </option>
        )
      }
    </select>
  </div>
);

export default DropDown;
