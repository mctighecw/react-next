import Head from 'next/head';
import Layout from '../Layout/Layout';
import '../../styles/global.less';

const PageTemplate = props => (
  <div>
    <Head>
      <title>React | Next</title>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <link href="https://fonts.googleapis.com/css?family=Merriweather" rel="stylesheet" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <Layout>
      {props.children}
    </Layout>
  </div>
);

export default PageTemplate;
