const express = require('express');
const next = require('next');

const dev = process.env.NODE_ENV !== 'production';
const environment = dev ? "Development" : "Production";
const location = dev ? "at http://localhost:" : "on port ";
const port = process.env.PORT || 3000;

const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();

    server.get('/favicon.ico', (req, res) => (
       res.status(200).sendFile('favicon.ico', {root: __dirname + '/static/'})
     ));

    server.get('/post/:id', (req, res) => {
      res.redirect('/blog');
    });

    server.get('/show/:id', (req, res) => {
      res.redirect('/shows');
    });

    server.get('*', (req, res) => {
      return handle(req, res);
    });

    server.listen(port, err => {
      if (err) throw err;
      console.log(`${environment} server running ${location}${port}`);
    });
  })
  .catch(err => {
    console.error(err.stack);
    process.exit(1);
  });
