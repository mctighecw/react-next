const Show = ({ data }) => (
  <div>
    <h1>{data.name}</h1>
    <p>{data.summary.replace(/<[/]?p>/g, '')}</p>
    <img src={data.image.medium} />
  </div>
);

export default Show;
