import Link from 'next/link';
import fetch from 'isomorphic-unfetch';
import DropDown from '../../components/DropDown/DropDown';
import { showOptions } from '../../constants/data';
import './styles.less';

class Shows extends React.Component {
  state = {
    value: "",
    data: {},
    loading: false
  }

  handleChangeDropdown = (event) => {
    const { value } = event.target;
    this.setState({ value, data: {}, loading: true }, () => {
      this.handleGetShow();
    });
  }

  handleGetShow = async () => {
    const { value } = this.state;
    const url = `https://api.tvmaze.com/search/shows?q=${value}`;
    const res = await fetch(url)
    const data = await res.json();

    if (data) {
      this.setState({ data, loading: false });
    };
  }

  render() {
    const { value, data, loading } = this.state;

    return (
      <div>
        <h1>TV Shows</h1>

        <DropDown
          value={value}
          options={showOptions}
          onChangeMethod={this.handleChangeDropdown}
        />
        {loading &&
          <div className="loading">Loading...</div>
        }

        {Object.keys(data).length > 0 &&
          <h3>Results</h3>
        }

        <ul>
          {Object.keys(data).length > 0 &&
            data.map(({show}) =>
              <li key={show.id}>
                <Link as={`/show/${show.id}`} href={`/show?id=${show.id}`}>
                  <a>{show.name}</a>
                </Link>
              </li>
            )
          }
        </ul>
      </div>
    );
  }
}

export default Shows;
