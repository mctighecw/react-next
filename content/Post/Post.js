import { withRouter } from 'next/router';

const Post = withRouter(props => {
  const { title, text } = props.router.query;

  return (
    <div>
      <h1>{title}</h1>
      <p>{text}</p>
    </div>
  );
});

export default Post;
