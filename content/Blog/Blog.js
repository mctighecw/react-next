import PostLink from '../../components/PostLink/PostLink';
import { blogPosts } from '../../constants/data';

const Blog = () => (
  <div>
    <h1>Blog</h1>
    <ul>
      {blogPosts.map((item, index) => {
        const title = `${item.slice(0,30)}...`;

          return (
            <PostLink title={title} text={item} id={index} key={index} />
          );
        })
      }
    </ul>
  </div>
);

export default Blog;
