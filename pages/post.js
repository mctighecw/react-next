import PageTemplate from '../components/PageTemplate/PageTemplate';
import Post from '../content/Post/Post';

export default () => (
  <PageTemplate>
    <Post />
  </PageTemplate>
);
