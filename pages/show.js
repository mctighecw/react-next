import PageTemplate from '../components/PageTemplate/PageTemplate';
import { withRouter } from 'next/router';
import fetch from 'isomorphic-unfetch';
import Show from '../content/Show/Show';

const ShowPage = ({ data }) => (
  <PageTemplate>
    <Show data={data} />
  </PageTemplate>
);

ShowPage.getInitialProps = async (context) => {
  const { id } = context.query;
  const res = await fetch(`https://api.tvmaze.com/shows/${id}`);
  const data = await res.json();
  return { data };
};

export default ShowPage;
