import PageTemplate from '../components/PageTemplate/PageTemplate';
import About from '../content/About/About';

export default () => (
  <PageTemplate>
    <About />
  </PageTemplate>
);
