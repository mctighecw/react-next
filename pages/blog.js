import PageTemplate from '../components/PageTemplate/PageTemplate';
import Blog from '../content/Blog/Blog';

export default () => (
  <PageTemplate>
    <Blog />
  </PageTemplate>
);
