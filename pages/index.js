import PageTemplate from '../components/PageTemplate/PageTemplate';
import Home from '../content/Home/Home';

export default () => (
  <PageTemplate>
    <Home />
  </PageTemplate>
);
