import PageTemplate from '../components/PageTemplate/PageTemplate';
import Shows from '../content/Shows/Shows';

const ShowsPage = () => (
  <PageTemplate>
    <Shows />
  </PageTemplate>
);

export default ShowsPage;
